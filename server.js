var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

//Database
mongoose.connect('mongodb://localhost/walmartlog');

//Setting post get data
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var router = express.Router();
var LogisticsNetwork = require('./model/logisticsNetwork');
var calculateCost = require('./controllers/cost');
//-------------------------------------
// ROUTES FOR API
//-------------------------------------

router.get('/', function(req, res) {
    res.json({
        message: 'It\'s alive!'
    });
});

function Path(origin, destination, distance) {
    if(distance < 0 ){
        return;
    }
    this.origin = origin;
    this.destination = destination;
    this.distance = distance;
}

function convertLogisticsNetwork(logNet) {
    var logisticsNetworkStructure = [];
	var fields = logNet.split(" ");
    for (var i = fields.length - 1; i >= 0; i -= 3) {
        var path = new Path(fields[i-2].toLowerCase(), fields[i-1].toLowerCase(), fields[i])
        if(path){
            logisticsNetworkStructure.push(path);
        }
    }
	return logisticsNetworkStructure;
}

router.route('/maps')
    .post(function(req, res) {
        var logisticsNetwork = new LogisticsNetwork();
        logisticsNetwork.name = req.body.name;
        logisticsNetwork.routes = convertLogisticsNetwork(req.body.network);
        logisticsNetwork.save(function(err) {
            if (err)
                res.send(err);
            res.json({
                message: 'Map created!'
            });
        });
    })
    .get(function(req, res) {
        LogisticsNetwork.find(function(err, maps) {
            if (err)
                res.send(err);

            res.json(maps);
        });
    });

router.route('/maps/:map_name')
    .get(function(req, res) {
        LogisticsNetwork.find({name:req.params.map_name}).exec(function(err, logisticsNetwork) {
            if (err)
                res.send(err);
            res.json(logisticsNetwork);
        });
    });

router.route('/calculate_cost')
    .post(function(req, res) {
        LogisticsNetwork.find({name:req.body.map_name}).exec(function(err, logisticsNetwork) {
            if (err)
                res.send(err);
            if (!logisticsNetwork[0])
                 res.json("");
            var cost = calculateCost.calculate(logisticsNetwork[0]['routes'], req.body.origin, req.body.destination, req.body.autonomy, req.body.cost);
            res.json(cost);
        });
        
    });

app.use('/api', router);

var port = process.env.PORT || 8080;
app.listen(port);
console.log('Server started at port ' + port);