var chai = require('chai');
var assert = chai.assert,
    should = chai.should();
var minCost = require('../controllers/cost');

describe('s for the minimum path find and calculation', function() {

    //test cases
    describe('Email example', function() {
    	var mapPath = [{
            "origin": "a",
            destination: "b",
            "distance": 10
        }, {
            "origin": "b",
            destination: "d",
            "distance": 15
        }, {
            "origin": "a",
            destination: "c",
            "distance": 20
        }, {
            "origin": "c",
            destination: "d",
            "distance": 30
        }, {
            "origin": "b",
            destination: "e",
            "distance": 50
        }, {
            "origin": "d",
            destination: "e",
            "distance": 30
        }];

        describe('Origin does not exists in the graph', function() {
            it('Should return a empty path  with cost 0', function() {
                var minDistance = minCost.calculate(mapPath, "e", "a", 10, 2.5);
                should.exist(minDistance);
                minDistance.should.have.property('path').with.length(0);
                minDistance.should.have.property('cost').to.equal(0);
            });
        });

        describe('Destination chosen do not have any path reaching then', function() {
            it('should return a empty path with cost 0', function() {
                var minDistance = minCost.calculate(mapPath, "b", "a", 10, 2.5);
                should.exist(minDistance);
                minDistance.should.have.property('path').with.length(0);
                minDistance.should.have.property('cost').to.equal(0);
            });
        });

        describe('Finding a path', function() {
            it('should return thje only existing path: A B with the distance of 10', function() {
                var minDistance = minCost.calculate(mapPath, "a", "d", 10, 2.5);
                should.exist(minDistance);
                minDistance.should.have.property('path').with.length(3);
                minDistance.should.have.property('cost').to.equal(6.25);
                minDistance['path'][0].should.equal('a');
                minDistance['path'][1].should.equal('b');
                minDistance['path'][2].should.equal('d');
            });
        });
    });
});