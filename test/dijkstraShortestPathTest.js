var chai = require('chai');
var assert = chai.assert,
    should = chai.should();
var distanceMatrix = require('../controllers/dijkstraShortestPath');

describe('s for the minimum path find and calculation', function() {

    //test cases
    describe('Minimal graph with only two nodes', function() {
        var mapPath = [{
            "origin": "a",
            destination: "b",
            "distance": 10
        }];

        describe('Origin does not exists in the graph', function() {
            it('Should return a empty route', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "e", "a");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(0);
                minDistance.should.have.property('distance').to.equal(null);
            });
        });

        describe('Destination chosen do not have any path reaching then', function() {
            it('should return a empty route', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "b", "a");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(0);
                minDistance.should.have.property('distance').to.equal(null);
            });
        });

        describe('Finding a route', function() {
            it('should return thje only existing route: A B with the distance of 10', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "b");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(2);
                minDistance.should.have.property('distance').to.equal(10);
                minDistance['route'][0].should.equal('a');
                minDistance['route'][1].should.equal('b');
            });
        });
    });

    describe('Simple for path node graph', function() {
        var mapPath = [{
            "origin": "a",
            destination: "b",
            "distance": 10
        }, {
            "origin": "a",
            destination: "c",
            "distance": 15
        }, {
            "origin": "b",
            destination: "d",
            "distance": 15
        }, {
            "origin": "c",
            destination: "d",
            "distance": 15
        }];
        describe('Finding the minimum route.', function() {
            it('should return a route A B D with the distance 25', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "d");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(3);
                minDistance.should.have.property('distance').to.equal(25);
                minDistance['route'][0].should.equal('a');
                minDistance['route'][1].should.equal('b');
                minDistance['route'][2].should.equal('d');
            });
        });
    });


    describe('Example from the problem description', function() {
        var mapPath = [{
            "origin": "a",
            destination: "b",
            "distance": 10
        }, {
            "origin": "b",
            destination: "d",
            "distance": 15
        }, {
            "origin": "a",
            destination: "c",
            "distance": 20
        }, {
            "origin": "c",
            destination: "d",
            "distance": 30
        }, {
            "origin": "b",
            destination: "e",
            "distance": 50
        }, {
            "origin": "d",
            destination: "e",
            "distance": 30
        }];

        describe('origin do not have path to destination', function() {
            it('should return a empty route when the orign do not have a route to that destination', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "c", "b");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(0);
                minDistance.should.have.property('distance').to.equal(null);
            });
        });

        describe('route get from the email A->D', function() {
            it('should return a exact minimum route', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "d");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(3);
                minDistance.should.have.property('distance').to.equal(25);
                minDistance['route'][0].should.equal('a');
                minDistance['route'][1].should.equal('b');
                minDistance['route'][2].should.equal('d');
            });
        });

        describe('Route A->E', function() {
            it('should return a exact minimum route', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "e");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(4);
                minDistance.should.have.property('distance').to.equal(55);
                minDistance['route'][0].should.equal('a');
                minDistance['route'][1].should.equal('b');
                minDistance['route'][2].should.equal('d');
                minDistance['route'][3].should.equal('e');
            });
        });
    });

    describe('Example 4', function() {
        var mapPath = [{
            "origin": "a",
            destination: "b",
            "distance": 1
        }, {
            "origin": "c",
            destination: "b",
            "distance": 5
        }, {
            "origin": "b",
            destination: "d",
            "distance": 1
        }, {
            "origin": "d",
            destination: "e",
            "distance": 1
        }, {
            "origin": "e",
            destination: "f",
            "distance": 2
        }, {
            "origin": "e",
            destination: "g",
            "distance": 1
        }, {
            "origin": "f",
            destination: "h",
            "distance": 1
        }, {
            "origin": "g",
            destination: "h",
            "distance": 3
        }];
        describe('Finding the minimum route.', function() {
            it('should return a route A B D E F H with the distance 6', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "h");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(6);
                minDistance.should.have.property('distance').to.equal(6);
                minDistance['route'][0].should.equal('a');
                minDistance['route'][1].should.equal('b');
                minDistance['route'][2].should.equal('d');
                minDistance['route'][3].should.equal('e');
                minDistance['route'][4].should.equal('f');
                minDistance['route'][5].should.equal('h');
            });
        });
    });

    describe('Example with cicle', function() {
        var mapPath = [{
            "origin": "a",
            destination: "b",
            "distance": 20
        }, {
            "origin": "a",
            destination: "d",
            "distance": 80
        }, {
            "origin": "a",
            destination: "g",
            "distance": 90
        }, {
            "origin": "b",
            destination: "f",
            "distance": 10
        }, {
            "origin": "c",
            destination: "f",
            "distance": 50
        }, {
            "origin": "c",
            destination: "h",
            "distance": 20
        }, {
            "origin": "c",
            destination: "d",
            "distance": 10
        }, {
            "origin": "d",
            destination: "c",
            "distance": 10
        },{
            "origin": "d",
            destination: "g",
            "distance": 20
        },{
            "origin": "e",
            destination: "g",
            "distance": 30
        },{
            "origin": "e",
            destination: "b",
            "distance": 50
        },{
            "origin": "f",
            destination: "d",
            "distance": 40
        },{
            "origin": "f",
            destination: "c",
            "distance": 10
        },{
            "origin": "g",
            destination: "a",
            "distance": 20
        }];
        describe('Finding the minimum route.', function() {
            it('should return a route with the distance 70', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "g");
                should.exist(minDistance);
                minDistance.should.have.property('distance').to.equal(70);
            });
            it('should return a route with the distance 50', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "d");
                should.exist(minDistance);
                minDistance.should.have.property('distance').to.equal(50);
            });
            it('should return a route with the distance 60', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "h");
                should.exist(minDistance);
                minDistance.should.have.property('distance').to.equal(60);
            });
            it('should return a empty route', function() {
                var minDistance = distanceMatrix.calculateShortestPath(mapPath, "a", "");
                should.exist(minDistance);
                minDistance.should.have.property('route').with.length(0);
                minDistance.should.have.property('distance').to.equal(null);
            });
        });
    });
});