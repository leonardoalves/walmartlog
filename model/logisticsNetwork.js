var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var logisticsNetworkSchema = new Schema({
	name: { type: String, required: true, unique: true },
	routes: [{ origin: String, destination: String, distance: Number}]
});

var LogisticsNetwork = mongoose.model('LogisticsNetwork', logisticsNetworkSchema);
module.exports = LogisticsNetwork;