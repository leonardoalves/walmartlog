//Protopyte to create the returns
function Path(route, distance) {
    this.route = route;
    this.distance = distance;
}

//check the origin is real valid origin, if there is no route using this point as origin
//there aren't any valid path from this node.
function existsTheOrgin(origin, logisticsNetwork) {
    for (var i = 0; i < logisticsNetwork.length; i++) {
        if (logisticsNetwork[i].origin == origin)
            return true;
    }
    return false
}

//Get destinations list
//This is used to validade if the distination is valid and to create a path distance
//vector
function getDestinations(logisticsNetwork) {
    var destinations = {}
    for (var i = 0; i < logisticsNetwork.length; i++) {
        destinations[logisticsNetwork[i].destination] = null;
    }
    return destinations;
}

//Select the unvisited node with the smaller distance.
function smallestCostNode(distances, visitedNodes) {
    var value;
    var node;
    Object.keys(distances).forEach(function(key) {
        if (visitedNodes.indexOf(key) == -1 && (distances[key] != null)) {
            if (!value && !node) {
                value = distances[key];
                node = key;
            }
            if (distances[key] <= value) {
                value = distances[key];
                node = key;
            }
        }
    });
    return node;
}

//Get all the vertices of one node
function getNodePaths(logisticsNetwork, origin) {
    var paths = []
    for (var i = logisticsNetwork.length - 1; i >= 0; i--) {
        if (logisticsNetwork[i].origin == origin) {
            paths.push(i);
        }
    }
    return paths;
}

//Trace back all the nodes from the destination to origin and return a array in order.
function minimumRoute(predecessorNode, destination, origin) {
    var path = [];
    var node = destination;
    path.push(destination);
    while (predecessorNode[node] && predecessorNode[node] != origin) {
        path.push(predecessorNode[node]);
        node = predecessorNode[node];
    }
    path.push(origin);
    return path.reverse();
}

//Executes the djkstra algorithm
function dijkstra(logisticsNetwork, distances, destination) {
    var visitedNodes = [];
    var predecessorNode = {};
    var origin = smallestCostNode(distances, visitedNodes);
    var smallest = origin; //The first smallest is the origin node.
    while (smallest) {
        var nodePaths = getNodePaths(logisticsNetwork, smallest);
        for (var index = nodePaths.length - 1; index >= 0; index--) {
            var actualDistance = distances[logisticsNetwork[nodePaths[index]].destination];
            var newDistance = distances[smallest] + logisticsNetwork[nodePaths[index]].distance;
            if (!actualDistance || actualDistance > newDistance) { //Update the new distance in the distances vector
                distances[logisticsNetwork[nodePaths[index]].destination] = newDistance;
                predecessorNode[logisticsNetwork[nodePaths[index]].destination] = smallest;
            }
        }
        visitedNodes.push(smallest);
        var smallest = smallestCostNode(distances, visitedNodes);
    };
    var route = minimumRoute(predecessorNode, destination, origin);
    return new Path(route, distances[destination]);
}

//Check the basics needs for a real response: -The origin exits and there is any path from origin node
function calculateShortestPath(logisticsNetwork, origin, destination) {
    if (!existsTheOrgin(origin, logisticsNetwork)) {
        return new Path([], null);
    }
    var distances = getDestinations(logisticsNetwork);
    if (distances.length < 1 || !distances.hasOwnProperty(destination)) {
        return new Path([], null);
    }
    distances[origin] = 0;
    var minPath = dijkstra(logisticsNetwork, distances, destination);
    if (minPath['distance'] > 0) {
        return minPath;
    } else {
        return new Path([], null);
    }
}

module.exports = {
    calculateShortestPath: function(logisticsNetwork, origin, destination) {
        return calculateShortestPath(logisticsNetwork, origin, destination);
    }
}