var shortestPath = require('../controllers/dijkstraShortestPath');

//Prototype for the response.
function shortestRoute(mapPath, origin, destination, autonomy, cost) {
	var route = shortestPath.calculateShortestPath(mapPath, origin, destination);
	this.path = route.route;
	this.cost = (route.distance/autonomy)*cost;
}

module.exports = {
	calculate: function (mapPath, origin, destination, autonomy, cost) {
		return new shortestRoute(mapPath, origin, destination, autonomy, cost);
	},
}