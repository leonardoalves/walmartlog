Introduction:
	By definition, of the problem i assume that is a directed grart shortest path problem. The decision of a graph with dierection was taken in base of the term used in the description of the logistics network: origin and destiantion.
	The problem was solved using Dijkstras's algorithm, finding the sortest path from a node to another. For time constrains and code readability I choose to not to use Fibinacci heap in this algorithm. Floyd-Warshall was not chosen because I assume that the distance between two cities in kilometers can not be negative.
	For the input, the only solution that I found for the logistics network was to ignore the line breaks. In a Json, form-input (used in this project) or XML wasn't any clear solution to load that. I could use the multiform to send a file, but this dificulties a lot the tests and usability of the API, because of dis it was simplified to a fom-input.
	This solution was implemented in Node.js because the simplistic and elegant asynchronous event-driven and built-in server. MongoDB was used to store the cities maps, a really low-overhead database.
		
Setup
	First you need to have Node.js and MongoDB intalled. 
	Use "npm install" to install all dependeces.
	Make sure that your shure that the port 8080 is free and MongoDB is running.

Run
	You can simply start the server using the command "node server.js"
	The server have some endpoints to allow the user iteration.

Endpoints
	/api/maps/ - This enpoint manages the maps and theirs logistics networks. There a brutal diferences depenging of the HTTP method used in the call.
		POST: Generate a new map, using the attribute "name" for a name for the map and "network" to receive the networkMap in a simple text data separated by spaces between the data. Answer example: {"message":"Map created!"} Post Example: map_post.png
		GET: return a JSON with a list that contains all maps.
	/api/maps/:map_name - GET - Return a JSON with a map, searching by name.
	/calculate_cost - POST - Calculate the minimum cost of a path between two points using the inputs: map_name, origin, destination, autonomy, cost. Returns a JSON with the total cost and the path used. Answer example: {"path":["a","b","d"],"cost":6.25} Post Example: calculate_cost.png

Tests
	You can run tests executing "npm test" in the root of this project.